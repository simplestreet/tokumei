<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php
		echo $this->Html->meta(
		    'description',
		    '匿名ツイートは、うれしかった事、悔しかったこと、なんでもいいので暇なときにツイートを投稿する場です。'
		);
	?>
	<?php
		echo $this->Html->meta('icon');

		/* echo $this->Html->css('cake.generic'); */
		echo $this->Html->css('base');
		echo $this->Html->css('common');
		echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
</head>
<body>
	<div id="container">
		<div id="header">
			<div id="header-inner">
				<h1><?php /*echo $this->Html->link('Home', '/');*/ ?></h1>
				<ul>
					<li><i class="fa fa-comments"></i> <?php echo $this->Html->link('匿名ツイート', '/'); ?></li>
					<li><i class="fa fa-comment"></i> <?php echo $this->Html->link('履歴',array('controller' =>'posts','action' =>'history') )?></li>
				</ul>
			<!-- #header-inner --></div>
		</div>
		<div id="content" class="clearfix">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			<?php if(0) : ?>
			<?php echo $this->Html->link(
					$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
					'http://www.cakephp.org/',
					array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
				);
			?>
			<p>
				<?php echo $cakeVersion; ?>
			</p>
			<?php endif; ?>
			<ul>
				<li><?php echo $this->Html->link('お問い合わせ',array('controller' =>'posts','action' =>'contact')); ?></li>
				<!-- <li><a href="/posts/contact">お問い合わせ</a></li> -->
				<li><a href="#">利用規約</a></li>
				<li><a href="#">プライバシーポリシー</a></li>
			</ul>
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
	<script>
		$(function() {
			setTimeout(function(){
				$('#flashMessage').fadeOut('slow');
			},800);
		});
	</script>
</body>
</html>
