<?php
function existOuen($ouens,$cookie_id){
	foreach($ouens as $ouen){
		if(strcmp($ouen['cookie_id'],$cookie_id) === 0){
			return true;
		}
	}
	return false;
}
?>
<div id="mainsection">
	<div id="gnav" class="clearfix">
		<p>
			<i class="fa fa-comment"></i> <span>マイ履歴</span>
			<span class="order-nav">
				<?php echo $this->Html->link(' ← ホームへ戻る',array('controller' =>'posts','action' =>'index','index')); ?>
			</span>
		</p>

		<div class="button">
			<?php echo $this->Html->link('つぶやく',array('controller' =>'posts','action' =>'add') )?>
		</div>
	</div>
	<div id="main">
		<ul>
			<?php $post_cnt = 0; ?>
			<?php foreach($posts as $post) : ?>
			<?php $post_cnt++; ?>
			<?php if($post_cnt === 6) : ?>
				<li id="post_ad" class="post_box">
					<div class="adsense_pt2">
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<!-- shukatsu_std_1 -->
						<ins class="adsbygoogle"
						     style="display:inline-block;width:300px;height:250px"
						     data-ad-client="ca-pub-7393795767652133"
						     data-ad-slot="2461227602"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					<!-- .adsense_pt2 --></div>
				</li>
			<?php else : ?>
				<li id="post_<?php echo h($post['Post']['id']); ?>" class="post_box">
					<div class="post_box_inner">
						<div class="post_top">
							<?php
								echo $this->Html->link($post['Post']['created'],'/posts/view/'.$post['Post']['id']);
							?>
							<span class="separator">·</span>
							<span class="unique_id">ID : <?php echo h($post['Post']['display_id'] ? $post['Post']['display_id'] : "anonymous"); ?></span>
						<!-- /.post_top --></div>
						<div class="post_body">
							<h2>
								<?php
								echo $this->Html->link($post['Post']['body'],'/posts/view/'.$post['Post']['id']);
								?>
							</h2>
						<!-- /.post_body --></div>
						<div class="post_action">
							<?php if(existOuen($post['Ouen'],$cookieKey)) : ?>
								<?php echo $this->Html->link('<i class="fa fa-heart"></i> 共感! <span class="num">'.count($post['Ouen']).'</span>','#',array('class'=>'ouen disable','data-post-id'=>$post['Post']['id'],'escape'=> false)); ?> 
							<?php else : ?>
								<?php echo $this->Html->link('<i class="fa fa-heart"></i> 共感! <span class="num">'.count($post['Ouen']).'</span>','#',array('class'=>'ouen','data-post-id'=>$post['Post']['id'],'escape'=> false)); ?> 
							<?php endif; ?>
							<?php if($post['Post']['refuse_comment'] === false) : ?>
								<span class="separator">·</span>
								<?php echo $this->Html->link('<i class="fa fa-pencil-square"></i> '.count($post['Comment']).' コメ','/posts/view/'.$post['Post']['id'],array('class'=>'comme','escape'=> false)); ?> 
							<?php endif; ?>
							<span class="separator">·</span>
							<?php
								if($post['Post']['cookie_id'] === $cookieKey){
									echo $this->Html->link('<i class="fa fa-trash-o"></i> 削除','#',array('class'=>'delete','data-post-id'=>$post['Post']['id'],'escape'=> false));
								}
							?>
						<!-- /.post_action --></div>
					<!-- /.post_box_inner --></div>
				</li>
			<?php endif; ?>
			<?php endforeach; ?>
		</ul>
		<div id="pagenation">
			<?php
				echo $this->Paginator->prev('← 前の20件', array(), null, array('class' => 'prev disabled'));
				echo $this->Paginator->next('次の20件 →', array(), null, array('class' => 'next disabled'));
			?>
		<!-- #pagenation --></div>
		<div class="adsense_pt3">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- shukatsu_std_1 -->
			<ins class="adsbygoogle"
			     style="display:inline-block;width:300px;height:250px"
			     data-ad-client="ca-pub-7393795767652133"
				     data-ad-slot="2461227602"></ins>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		<!-- .adsense_pt3 --></div>
	<!-- #main --></div>
<!-- mainsection --></div>
<div id="sub">
	<div class="adsense_pt1">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- shukatsu_std_2 -->
		<ins class="adsbygoogle"
		     style="display:inline-block;width:300px;height:250px"
		     data-ad-client="ca-pub-7393795767652133"
		     data-ad-slot="3937960805"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	<!-- .adsense_pt1 --></div>
	<div class="explanation">
		<h3>就活のぐちとは</h3>
		<p class="description">就活のぐちは、就職活動に関する愚痴をみんなで共感し励ましあう匿名つぶやきサービスです。</p>
		<div class="button">
			<?php echo $this->Html->link('<i class="fa fa-comment"></i> 匿名でつぶやく',array('controller' =>'posts','action' =>'add'),array('escape' => false) )?>
		<!-- /.button--></div>
	<!-- .explanation --></div>
	<div class="adsense_pt1">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- shukatsu_std_3 -->
		<ins class="adsbygoogle"
		     style="display:inline-block;width:300px;height:250px"
		     data-ad-client="ca-pub-7393795767652133"
		     data-ad-slot="5414694003"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	<!-- .adsense_pt1 --></div>
<!-- #sub --></div>

<script>
	$(function(){
		$('a.delete').click(function(e){
			if(confirm('削除しますか?')){
				$.post('/posts/delete/'+$(this).data('post-id'),{},function(res){
					$('#post_'+res.id).fadeOut();
				},"json");
			}
			return false;
		});
		$('a.ouen').click(function(e){
			$.post('/ouens/add/'+$(this).data('post-id'),{},function(res){
				var xxx = res.count;
				if(xxx > 0 ){
					$('a.ouen[data-post-id='+ res.id +'],a.ouen[data-post-id='+ res.id +'] .num ').css({"color":"#888888","pointer-events":"none","text-decoration":"none","cursor": "default"});
					$('a.ouen[data-post-id='+ res.id +'] ' + 'span.num').html(res.count);
				}
			},"json");
			return false;
		});
	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-62866190-3', 'auto');
  ga('send', 'pageview');
</script>
