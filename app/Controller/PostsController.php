<?php

class PostsController extends AppController {
	public $helpers = array('Html','Form');
	var $components  = array('Cookie');
	private $idLength = 12;
	private $displayIdLength = 6;
	public $paginate = array(
				'limit' => 20,
				'order' => array(
					'Post.created' => 'desc'
				)
			);
	function __cookieId($length) {
		$str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'), range(':', '@'));
		$r_str = null;
		for ($i = 0; $i < $length; $i++) {
			$r_str .= $str[rand(0, count($str)-1)];
		}
		return $r_str;
	}
	function __cookieCheck($length){
		if(!$this->Cookie->check('gutiCookie')){
			$this->Cookie->write('gutiCookie', $this->__cookieId($length),true,60*60*24*365*20 /*"20 year"*/);
		}
	}
	public function index($mode = null){
		if(strcmp($mode,'update') === 0) {
			$this->paginate = array(
				'limit' => 20,
				'order' => array(
					'Post.modified' => 'desc'
				)
			);
			$this->set('gnav_mode','update');
		} elseif(strcmp($mode,'popular') === 0) {
			$this->loadModel('Ouen');
			/* 24時間以内の応援が多い順番 */
			$params = $this->Ouen->find('all',array(
				'fields' => array('Ouen.post_id','count(Ouen.post_id) as cnt'),
				'conditions' => array('date_add(Ouen.created,interval 1 day) >= now()'),
				'order' => array('cnt desc'),
				'group' => array('Ouen.post_id'),
				'limit' => 20
			));
			if(empty($params)){
				$this->paginate = array(
					'conditions' => array('Post.id' => $popular_ids),
					'limit' => 0
				);
			}else{
				$popular_ids = array();
				foreach($params as $param){
					array_push($popular_ids,$param['Ouen']['post_id']);
				}
				$this->paginate = array(
					'conditions' => array('Post.id' => $popular_ids),
					'limit' => 20,
					'order' => 'FIELD(Post.id,'.implode(',',$popular_ids).')'
				);
			}
			$this->set('gnav_mode','popular');
		} else {
			$this->paginate = array(
				'limit' => 20,
				'order' => array(
					'Post.created' => 'desc'
				)
			);
			$this->set('gnav_mode','home');
		}
		$this->__cookieCheck($this->idLength);
		$this->set('posts',$this->paginate());
		$this->set('cookieKey',$this->Cookie->read('gutiCookie'));
		$this->set('title_for_layout','匿名ツイート - ツイートをみんなで共感しあう匿名つぶやきサービス');
	}
	public function beforeFilter(){
		parent::beforeFilter();
		if($this->Session->read('errors')){
			foreach($this->Session->read('errors') as $model => $errors){
				$this->loadModel($model);
				/*$this->$model->validationErrors = $errors;*/
				$this->set( 'valerror',$errors);
			}
			$this->Session->delete('errors');
		}
	}
	public function contact(){
		$this->set('title_for_layout','お問い合わせ - 匿名ツイート');
	}
	public function view($id = null) {
		/* configure of cookie */
		$this->__cookieCheck($this->idLength);
		$this->set('cookieKey',$this->Cookie->read('gutiCookie'));
		
		$this->Post->id = $id;
		/*$this->set('post',$this->Post->read());*/
		$param = array(
			'conditions' => array('Post.id' => $id),
			'recursive' => 2
		);
		$data = $this->Post->find('first',$param);
		$this->set('post',$data);
		$this->set('title_for_layout',$data['Post']['body'].' - 匿名ツイート');
	}
	
	public function add() {
		/* configure of cookie */
		$this->__cookieCheck($this->idLength);
		$this->set('cookieKey',$this->Cookie->read('gutiCookie'));
		$this->set('title_for_layout','つぶやく - 匿名ツイート');
		if($this->request->is('post')) {
			$this->request->data['Post']['cookie_id'] = $this->Cookie->read('gutiCookie');
			$this->request->data['Post']['display_id'] = $this->__cookieId($this->displayIdLength);
			$this->request->data['Post']['ip'] = $_SERVER['REMOTE_ADDR'];
			if($this->Post->save($this->request->data)) {
				$this->redirect(array('action'=>'index'));
			} else {
			}
		}
	}
	public function history() {
		/* configure of cookie */
		$this->__cookieCheck($this->idLength);
		$this->set('cookieKey',$this->Cookie->read('gutiCookie'));
		$this->set('title_for_layout','マイ履歴 - 匿名ツイート');
		
		$conditions = array("Post.cookie_id" => $this->Cookie->read('gutiCookie'));
		$this->set('posts',$this->paginate($conditions));
	}

	public function edit($id = null){
		$this->Post->id=$id;
		if($this->request->is('get')) {
			$this->request->data = $this->Post->read();
		} else {
			if($this->Post->save($this->request->data)){
				$this->Session->setFlash('Success!');
				$this->redirect(array('action' => 'index'));
			}else{
				$this->Session->setFlash('failed!');
			}
		}
	}
	public function delete($id = null){
		if($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		if($this->request->is('ajax')) {
			if($this->Post->delete($id)){
				$this->autoRender = false;
				$this->autoLayout = false;
				$response = array('id' => $id);
				$this->header('Content-Type: application/json');
				echo json_encode($response);
				exit();
			}
		}
		$this->redirect(array('action'=>'index'));
	}
}

