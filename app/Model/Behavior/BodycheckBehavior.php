<?php

class BodycheckBehavior extends ModelBehavior {
	public static function rules(){
		$rules = array(
			'body' => array(
				'rule1' => array(
					'rule' => 'notOnlyAlphaNumericSymbol',
					'message' => '英数字のみで投稿することはできません。'
				),
				'rule2' => array(
					'rule' => 'notUrl',
					'message' => '本文にURLを含むことはできません。'
				),
				'rule3' => array(
					'rule' => array('maxLength',140),
					'message' => '140文字を超えています。'
				),
				'rule4' => array(
					'rule' => 'notReasonableWriting',
					'message' => '使用が禁止されているNGワードが含まれています。'
				)
			)
		);
		return $rules;
	}
	/* 日本語が含まれていないかチェックする。 */
	public function notOnlyAlphaNumericSymbol($model,$check){
		$value = array_values($check);
		$value = $value[0];
		if(preg_match('|^[!-~ ]*$|', $value)){
			return false;
		}else{
			return true;
		}
	}
	/* NGワードチェック */
	public function notReasonableWriting($model,$check){
		$value = array_values($check);
		$value = $value[0];
		$pattern = '/ちんぽ|セクロス|ｾｸﾛｽ|ｾｯｸｽ|セックス|SEX|sex|フェラ|ﾌｪﾗ|キチガイ|デリヘル|クリトリス|くりとりす|殺した|殺す|風俗|ぞくふー|ゾクフー|勃起|精子|ちんこ|チンコ|ﾁﾝｺ|まんこ|マンコ|ﾏﾝｺ|死ね|氏ね|詩ね/u';
		if(preg_match($pattern, $value)){
			return false;
		}else{
			return true;
		}
	}
	public function notUrl($model,$check){
		//debug($check);
		if(stristr($check['body'],"http")) {
			return false;
		} else {
			return true;
		}
	}
}

