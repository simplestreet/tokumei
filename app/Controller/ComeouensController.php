<?php

class ComeouensController extends AppController {
	public $helpers = array('Html','Form');
	var $components  = array('Cookie');
	public function add($id = null){
		if($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		if($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->autoLayout = false;
			
			$data = array('Comeouen' => array('cookie_id' => $this->Cookie->read('gutiCookie'),'comment_id' => $id));
			$fields = array('cookie_id','comment_id');
			/* なぜだかExceptionになる。*/
			try{
				if($this->Comeouen->save($data,false,$fields)) {
					$count = $this->Comeouen->find('count' , array(
						'conditions' => array('comment_id' => $id)
					));
					$response = array('count' => $count,'id' => $id);
				}else{
					$response = array('count' => -1);
				}
			}catch(Exception $e){
				$response = array('count' => -1);
				$this->header('Content-Type: application/json');
				echo json_encode($response);
				exit();
			}
			$this->header('Content-Type: application/json');
			echo json_encode($response);
			exit();
		}
		/*$this->redirect(array('controller'=>'posts','action'=>'index'));*/
	}
}
