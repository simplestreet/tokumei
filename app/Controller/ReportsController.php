<?php

class ReportsController extends AppController {
	/* public $helpers = array('Html','Form'); */
	var $components  = array('Cookie');
	public function add($id = null){
		if($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		if($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->autoLayout = false;
			$data = array('Report' => array('cookie_id' => $this->Cookie->read('gutiCookie'),'post_id' => $id));
			$fields = array('cookie_id','post_id');
			/* なぜだかExceptionになる。*/
			try{
				if($this->Report->save($data,false,$fields)) {
					$response = array('id' => $id);
				}else{
					$response = array('id' => 0);
				}
			}catch(Exception $e){
				$response = array('id' => 0);
				$this->header('Content-Type: application/json');
				echo json_encode($response);
				exit();
			}
			$this->header('Content-Type: application/json');
			echo json_encode($response);
			exit();
		}
	}
}
