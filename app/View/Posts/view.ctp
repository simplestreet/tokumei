<?php
function existOuen($ouens,$cookie_id){
	foreach($ouens as $ouen){
		if(strcmp($ouen['cookie_id'],$cookie_id) === 0){
			return true;
		}
	}
	return false;
}
function existReport($reports,$cookie_id){
	foreach($reports as $report){
		if(strcmp($report['cookie_id'],$cookie_id) === 0){
			return true;
		}
	}
	return false;
}
?>
<div id="mainsection">
	<div id="main">
		<ul>
			<?php if ($post['Post']['refuse_comment'] === false) : ?>
				<li id="post_<?php echo h($post['Post']['id']); ?>" class="post_box">
			<?php else : ?>
				<li id="post_<?php echo h($post['Post']['id']); ?>" class="post_box noborder">
			<?php endif; ?>
				<div class="post_box_inner">
					<div class="post_top">
						<?php
							echo $this->Html->link('1: '.$post['Post']['created'],'/posts/view/'.$post['Post']['id']);
						?>
						<span class="separator">·</span>
						<span class="unique_id">ID : <?php echo h($post['Post']['display_id'] ? $post['Post']['display_id'] : "anonymous"); ?></span>
					<!-- /.post_top --></div>
					<div class="post_body">
						 <?php if(!existReport($post['Report'],$cookieKey)): ?>
							<h1>
								<?php echo nl2br(h($post['Post']['body'])); ?>
							</h1>
						<?php else : ?>
							<h1 class="reported">
								このつぶやきは報告済みです。
							</h1>
						<?php endif; ?>
					<!-- /.post_body --></div>
					<div class="post_action">
						<?php if(existOuen($post['Ouen'],$cookieKey)) : ?>
							<?php echo $this->Html->link('<i class="fa fa-heart"></i> 共感! <span class="num">'.count($post['Ouen']).'</span>','#',array('class'=>'ouen disable','data-post-id'=>$post['Post']['id'],'escape'=> false)); ?> 
						<?php else : ?>
							<?php echo $this->Html->link('<i class="fa fa-heart"></i> 共感! <span class="num">'.count($post['Ouen']).'</span>','#',array('class'=>'ouen','data-post-id'=>$post['Post']['id'],'escape'=> false)); ?> 
						<?php endif; ?>
						<span class="separator">·</span>
						<?php
							if($post['Post']['cookie_id'] === $cookieKey){
								echo $this->Html->link('<i class="fa fa-trash-o"></i> 削除','#',array('class'=>'delete','data-post-id'=>$post['Post']['id'],'escape'=> false));
							}else{
								if(count($post['Report']) == 0){
									echo $this->Html->link('<i class="fa fa-frown-o"></i> 報告','#',array('class'=>'report','data-post-id'=>$post['Post']['id'],'escape'=> false));
								}
							}
						?>
					<!-- /.post_action --></div>
					<?php if($post['Post']['refuse_comment'] === false) : ?>
						<div class="comment-section">
							<a class="flat-button" href="#comment_form">コメントする</a>
						</div>
					<?php endif; ?>
				<!-- /.post_box_inner --></div>
				<?php if($post['Post']['refuse_comment'] === false) : ?>
					<div class="adsense_view_pt1">
						<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<!-- shukatsu_view_1 -->
						<ins class="adsbygoogle"
						     style="display:inline-block;width:320px;height:100px"
						     data-ad-client="ca-pub-7393795767652133"
						     data-ad-slot="8655218402"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>
					<!-- .adsense_view_pt1 --></div>
				<?php endif; ?>
			</li>
			<?php if(count($post['Comment']) !== 0 ) :?>
				<li class="comment_header">
					<h3>みんなのコメント <?php echo h(count($post['Comment']));?> 件</h3>
				</li>
				<?php $comment_index = 2; ?>
				<?php foreach($post['Comment'] as $comment):?>
					<?php if($comment['display'] === true ) : ?>
						<li id="comment_<?php echo h($comment['id']); ?>" class="comment_box">
							<div class="comment_box_inner">
								<div class="comment_top">
									<?php
										echo $this->Html->link($comment_index.': '.$comment['created'],'#'.$comment['id']);
										$comment_index++;
									?>
									<span class="separator">·</span>
									<?php if($post['Post']['cookie_id'] === $comment['cookie_id']) : ?>
										<span class="unique_id">ID : <?php echo h('主'); ?></span>
									<?php else : ?>
										<span class="unique_id">ID : <?php echo h($comment['cookie_id']); ?></span>
										<?php 
											if($post['Post']['cookie_id'] === $cookieKey ) {
												echo $this->Html->link('<i class="fa fa-times"></i> ブロック','#',array('class'=>'comment_block','data-comment-id'=>$comment['id'],'escape'=> false));
											}
										?>
									<?php endif; ?>
								<!-- /.comment_top --></div>
								<div class="comment_body">
									<h1>
										<?php echo nl2br(h($comment['body'])); ?>
									</h1>
								<!-- /.comment_body --></div>
								<div class="comment_action">
									<?php if(existOuen($comment['Comeouen'],$cookieKey)) : ?>
										<?php echo $this->Html->link('<i class="fa fa-heart"></i> 共感! <span class="num">'.count($comment['Comeouen']).'</span>','#',array('class'=>'comeouen disable','data-comment-id'=>$comment['id'],'escape'=> false)); ?> 
									<?php else : ?>
										<?php echo $this->Html->link('<i class="fa fa-heart"></i> 共感! <span class="num">'.count($comment['Comeouen']).'</span>','#',array('class'=>'comeouen','data-comment-id'=>$comment['id'],'escape'=> false)); ?> 
									<?php endif; ?>
									<span class="separator">·</span>
									<a href="#comment_form" class="reply" data-reply-id="<?php echo $comment_index-1 ;?>"><i class="fa fa-share"></i> 返信する</a>
									<span class="separator">·</span>
									<?php
										if($comment['cookie_id'] === $cookieKey){
											echo $this->Html->link('<i class="fa fa-trash-o"></i> 削除','#',array('class'=>'delete','data-comment-id'=>$comment['id'],'escape'=> false));
										}
									?>
								<!-- /.comment_action --></div>
							<!-- /.comment_box_inner --></div>
						</li>
					<?php else : ?>
						<li id="comment_<?php echo h($comment['id']); ?>" class="comment_box deleted">
							<div class="comment_box_inner">
								<p><?php echo h($comment_index.': '); ?>削除</p>
								<?php $comment_index++; ?>
							</div>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif;?>
		</ul>
		
		<?php if($post['Post']['refuse_comment'] === false) : ?>
			<div id="comment_form">
				<p>共感・励ましのコメントをどうぞ : あと<span id="txtmax">140</span>文字</p>
				<?php if(!empty($valerror)) : ?>
					<p class="error"><?php echo h($valerror['body'][0]); ?></p>
				<?php endif; ?>
				<!-- <p class="error"><?php /* if(!empty($valerror)){
				 	echo h($valerror['body'][0]);
				}
				*/
				?></p> -->
				<?php
					echo $this->Form->create('Comment',array('action'=>'add'));
					echo $this->Form->input('body',array('cols'=> 40 ,'rows'=> 7 ,'label'=>''));
					echo $this->Form->input('Comment.post_id',array('type'=>'hidden','value'=>$post['Post']['id']));
				?>
				<div class="caution">
					性的な表現や個人名を含むつぶやき（伏字や表記を変えているものも含む）を投稿した場合、投稿制限を行う場合があり、制限中に投稿されたつぶやきはトップページに表示されませんのでご注意ください。
				<!-- .caution --></div>
				<?php
					echo $this->Form->end('投稿する');
				?>
			<!-- #cooment_form --></div>
		<?php endif; ?>
	<!-- #main --></div>
<!-- #mainsection --></div>
<div id="sub">
	<div class="adsense_pt1">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- shukatsu_std_2 -->
		<ins class="adsbygoogle"
		     style="display:inline-block;width:300px;height:250px"
		     data-ad-client="ca-pub-7393795767652133"
		     data-ad-slot="3937960805"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	<!-- .adsense_pt1 --></div>
	<div class="adsense_pt1">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- shukatsu_std_3 -->
		<ins class="adsbygoogle"
		     style="display:inline-block;width:300px;height:250px"
		     data-ad-client="ca-pub-7393795767652133"
		     data-ad-slot="5414694003"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	<!-- .adsense_pt1 --></div>
<!-- #sub --></div>
<script>
	$(function(){
		$('a.ouen').click(function(e){
			$.post('/ouens/add/'+$(this).data('post-id'),{},function(res){
				var xxx = res.count;
				if(xxx > 0 ){
					$('a.ouen[data-post-id='+ res.id +'],a.ouen[data-post-id='+ res.id +'] .num ').css({"color":"#888888","pointer-events":"none","text-decoration":"none","cursor": "default"});
					$('a.ouen[data-post-id='+ res.id +'] ' + 'span.num').html(res.count);
				}
			},"json");
			return false;
		});
		$('a.comeouen').click(function(e){
			var yyy = $(this).data('comment-id');
			$.post('/comeouens/add/'+$(this).data('comment-id'),{},function(res){
				var xxx = res.count;
				if(xxx > 0 ){
					$('a.comeouen[data-comment-id='+ res.id +'],a.comeouen[data-comment-id='+ res.id +'] .num  ').css({"color":"#999999","pointer-events":"none","text-decoration":"none","cursor": "default"});
					$('a.comeouen[data-comment-id='+ res.id +'] ' + 'span.num').html(res.count);
				}
			},"json");
			return false;
		});
		$('.comment_action a.reply').click(function(e){
			var area = $('#CommentBody').val();
			$('#CommentBody').val("Re:" + $(this).data('reply-id') + "\n" + area);
			return true;
		});
		$('.post_action a.delete').click(function(e){
			if(confirm('削除しますか?')){
				$.post('/posts/delete/'+$(this).data('post-id'),{},function(res){
					$(location).attr("href", "/posts/history");
				},"json");
			}
			return false;
		});
		$('.comment_action a.delete').click(function(e){
			if(confirm('このコメントを削除しますか?')){
				$.post('/comments/delete/'+$(this).data('comment-id'),{},function(res){
					$('#comment_'+res.id).fadeOut();
				},"json");
					console.log('hello');
			}
			return false;
		});
		$('.post_action a.report').click(function(e){
			if(confirm('問題のあるつぶやきとして、報告してもよろしいですか?')){
				$.post('/reports/add/'+$(this).data('post-id'),{},function(res){
					if(res.id > 0 ){
						$('#post_'+res.id + ' .post_body h1').css('color','#BFA699').text('このつぶやきは報告済みです。');
						$('.post_action a.report').fadeOut();
					}
				},"json");
			}
			return false;
		});
		$('.comment_top a.comment_block').click(function(e){
			if(confirm('このコメントをブロックしますか?')){
				$.post('/comments/delete/'+$(this).data('comment-id'),{},function(res){
					$('#comment_'+res.id).fadeOut();
				},"json");
								console.log('hello');
			}
			return false;
		});
		//140文字カウンター
		$('#CommentBody').bind('keydown keyup change',function(){
			var tnum  = $(this).val().length;
			//$('#txtnum').text(tnum);
			var tmax = 140 - tnum;
			if(tmax > 0){
				$('#txtmax').text(tmax);
			}else{
				$('#txtmax').text(tmax);
			}
		});
	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-62866190-3', 'auto');
  ga('send', 'pageview');
</script>

