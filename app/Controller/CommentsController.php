<?php

class CommentsController extends AppController {
	public $helpers = array('Html','Form');
	var $components  = array('Cookie');
	public function add() {
		if($this->request->is('post')) {
			$this->request->data['Comment']['cookie_id'] = $this->Cookie->read('gutiCookie');
			$this->request->data['Comment']['ip'] = $_SERVER['REMOTE_ADDR'];
			if($this->Comment->save($this->request->data)) {
				$this->Session->setFlash('The comment has been posted!');
				//$this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
				$fields = array('Post.modified' => "'".date('Y-m-d H:i:s')."'" );
				$conditions = array('Post.id' => $this->request->data['Comment']['post_id'] );
				$this->loadModel('Post');
				$this->Post->updateAll($fields,$conditions);
			} else {
				$this->Session->write('errors.Comment',$this->Comment->validationErrors);
				$this->Session->setFlash('Failed to save!');
				//$this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
			}
		}
		$this->redirect(array('controller'=>'posts','action'=>'view',$this->data['Comment']['post_id']));
	}

	public function delete($id = null){
		if($this->request->is('get')) {
			throw new MethodNotAllowedException();
		}
		if($this->request->is('ajax')) {
			$fields = array('Comment.display' => 0 );
			$conditions = array('Comment.id' => $id );
			if( $this->Comment->updateAll($fields,$conditions)) {
			/* if($this->Comment->delete($id)){ */
			/*	$this->Comment->updateAll($fields,$conditions); */
				$this->autoRender = false;
				$this->autoLayout = false;
				$response = array('id' => $id);
				$this->header('Content-Type: application/json');
				echo json_encode($response);
				exit();
			}
		}
		$this->redirect(array('controller'=>'posts','action'=>'index'));
	}
}

