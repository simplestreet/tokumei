<div id="mainsection">
	<div id="main">
		<h2 class="tubuyaku">つぶやく</h2>
		<div id="tubuyaki">
			<p>内容をどうぞ : あと<span id="txtmax">140</span>文字</p>
			<?php
				echo $this->Form->create('Post');
				echo $this->Form->input('body',array('cols'=> 40 ,'rows'=> 7 ,'label'=>''));
			?>
			<div class="caution">
				性的な表現や個人名を含むつぶやき（伏字や表記を変えているものも含む）を投稿した場合、投稿制限を行う場合があり、制限中に投稿されたつぶやきはトップページに表示されませんのでご注意ください。
			<!-- .caution --></div>
			<?php
				echo $this->Form->input('refuse_comment',array('type'=>'checkbox','label'=>' コメント受け付けない'));
			?>
			<?php
				echo $this->Form->end('投稿する');
			?>
		<!-- #tubuyaki --></div>
	<!-- #main --></div>
<!-- #mainsection --></div>
<div id="sub">
	<div class="normal-item">
		<h3>ルール</h3>
		<ul>
			<li>匿名ツイートは、うれしかった事、悔しかったこと、なんでもいいので暇なときにツイートを投稿する場です。</li>
			<li>一方的な悪口や恨み辛み、個人を攻撃・馬鹿にするようなつぶやき、または卑猥な表現を含むつぶやきは投稿できません。節度のあるご利用をお願いいたします。</li>
			<li>匿名ツイート利用者や、特定のつぶやきに対する愚痴や批判、言及は投稿できません。</li>
			<li>不適切な表現を含むつぶやきは一覧に表示されません。</li>
		</ul>
	<!-- .normal-item --></div>
	<div class="normal-item">
		<h3 class="heading">機能紹介</h3>
		<ul>
			<li>投稿内容は削除することができます。</li>
			<li>つぶやきに投稿されたコメントは、あなたの権限で「ブロック」することができます。</li>
			<li>"コメントを受けつけない"にチェックを入れると、コメントを受け付けなくなります。</li>
		</ul>
	<!-- .normal-item --></div>
<!-- #sub --></div>
<script>
	$(function(){
		//140文字カウンター
		$('#PostBody').bind('keydown keyup change',function(){
			var tnum  = $(this).val().length;
			//$('#txtnum').text(tnum);
			var tmax = 140 - tnum;
			if(tmax > 0){
				$('#txtmax').text(tmax);
			}else{
				$('#txtmax').text(tmax);
			}
		});
	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62866190-3', 'auto');
  ga('send', 'pageview');

</script>
