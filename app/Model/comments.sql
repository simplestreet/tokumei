create table comments(
	id int not null auto_increment primary key,
	cookie_id varchar(16),
	body text,
	post_id int not null,
	display boolean default true,
	ip varchar(60) default null,
	created datetime default null,
	modified datetime default null
);

insert into comments (commenter,body,post_id,created,modified) values
('c 1','b 1',7,now(),now()),
('c 2','b 2',7,now(),now()),
('c 3','b 3',7,now(),now());
