create table posts(
	id int not null auto_increment primary key,
	cookie_id varchar(16),
	display_id varchar(7),
	body text,
	refuse_comment boolean default false,
	ip varchar(60) default null,
	created datetime default null,
	modified datetime default null
);
////
insert into posts (title,body,created,modified) values
('title 1',"body 1",now(),now()),
('title 2',"body 2",now(),now()),
('title 3',"body 3",now(),now());
